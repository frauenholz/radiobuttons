/** Dependencies */
import {
    MarkdownFeatures,
    NodeBlock,
    Slots,
    assert,
    findFirst,
    markdownifyToString,
} from "tripetto-runner-foundation";
import { IRadiobutton } from "./radiobutton";

export abstract class Radiobuttons extends NodeBlock<{
    readonly buttons?: IRadiobutton[];
    readonly randomize?: boolean;
}> {
    /** Contains the randomized buttons order. */
    private randomized?: {
        readonly index: number;
        readonly id: string;
    }[];

    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Contains the radio buttons slot. */
    readonly radioSlot = assert(
        this.valueOf<string, Slots.String>("button", "static", {
            confirm: true,
            modifier: (data) => {
                if (data.value) {
                    if (!data.reference) {
                        const selected =
                            findFirst(
                                this.props.buttons,
                                (button) => button.value === data.value
                            ) ||
                            findFirst(
                                this.props.buttons,
                                (button) => button.id === data.value
                            ) ||
                            findFirst(
                                this.props.buttons,
                                (button) => button.name === data.value
                            ) ||
                            findFirst(
                                this.props.buttons,
                                (button) =>
                                    button.name.toLowerCase() ===
                                    data.value.toLowerCase()
                            );

                        return {
                            value:
                                selected && (selected.value || selected.name),
                            reference: selected && selected.id,
                        };
                    } else if (
                        !findFirst(
                            this.props.buttons,
                            (button) => button.id === data.reference
                        )
                    ) {
                        return {
                            value: undefined,
                            reference: undefined,
                        };
                    }
                }
            },
            onChange: (slot) => {
                if (this.scoreSlot) {
                    const selected = findFirst(
                        this.props.buttons,
                        (button) => button.id === slot.reference
                    );

                    this.scoreSlot.set(selected && (selected?.score || 0));
                }
            },
        })
    );

    /** Contains if the radio button is required. */
    readonly required = this.radioSlot.slot.required || false;

    /** Retrieves the reference of the selected button. */
    get value() {
        const selected = findFirst(
            this.props.buttons,
            (button) => button.id === this.radioSlot.reference
        );

        if (
            !selected &&
            this.required &&
            this.props.buttons &&
            this.props.buttons.length > 0
        ) {
            return this.select(this.props.buttons[0]);
        }

        return (selected && selected.id) || "";
    }

    /** Sets the reference of the selected button. */
    set value(reference: string) {
        const selected = findFirst(
            this.props.buttons,
            (button) => button.id === reference
        );

        if (selected) {
            this.select(selected);
        }
    }

    /** Retrieves the checkboxes. */
    buttons<T>(props: {
        readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => T;
    }): (Omit<IRadiobutton, "description"> & {
        readonly label: T;
        readonly description?: T;
    })[] {
        const buttons =
            this.props.buttons?.map((button) => ({
                ...button,
                name: markdownifyToString(
                    button.name,
                    this.context,
                    undefined,
                    false,
                    MarkdownFeatures.Formatting | MarkdownFeatures.Hyperlinks
                ),
                label: props.markdownifyToJSX(button.name, false),
                description:
                    (button.description &&
                        props.markdownifyToJSX(button.description, true)) ||
                    undefined,
            })) || [];

        if (this.props.randomize && buttons.length > 1) {
            if (
                !this.randomized ||
                this.randomized.length !== buttons.length ||
                findFirst(
                    this.randomized,
                    (button) => buttons[button.index]?.id !== button.id
                )
            ) {
                this.randomized = buttons.map((button, index) => ({
                    index,
                    id: button.id,
                }));

                let length = this.randomized.length;

                while (--length) {
                    const index = Math.floor(Math.random() * length);
                    const temp = this.randomized[length];

                    this.randomized[length] = this.randomized[index];
                    this.randomized[index] = temp;
                }
            }

            return this.randomized.map((button) => buttons[button.index]);
        } else if (this.randomized) {
            this.randomized = undefined;
        }

        return buttons;
    }

    /** Verifies if a button is selected. */
    isSelected(button: IRadiobutton): boolean {
        return this.value === button.id;
    }

    /** Selects a button. */
    select(button: IRadiobutton): string {
        this.radioSlot.set(button.value || button.name, button.id);

        return button.id;
    }
}
