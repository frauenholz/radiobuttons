import {
    Collection,
    Forms,
    Slots,
    alias,
    definition,
    editor,
    insertVariable,
    isString,
    name,
    pgettext,
    score,
} from "tripetto";
import { Radiobuttons } from "./";

export class Radiobutton extends Collection.Item<Radiobuttons> {
    @definition("string")
    @name
    name = "";

    @definition("string", "optional")
    description?: string;

    @definition("string", "optional")
    @alias
    value?: string;

    @definition("number", "optional")
    @score
    score?: number;

    @editor
    defineEditor(): void {
        this.editor.option({
            name: "Name",
            form: {
                title: pgettext("block:radiobuttons", "Radio button name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect()
                        .enter(this.editor.close)
                        .escape(this.editor.close),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:radiobuttons", "Description"),
            form: {
                title: pgettext(
                    "block:radiobuttons",
                    "Radio button description"
                ),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "description", undefined)
                    ).action("@", insertVariable(this)),
                ],
            },
            activated: isString(this.description),
        });
        this.editor.group(pgettext("block:radiobuttons", "Options"));
        this.editor.option({
            name: pgettext("block:radiobuttons", "Identifier"),
            form: {
                title: pgettext("block:radiobuttons", "Identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:radiobuttons",
                            // tslint:disable-next-line:max-line-length
                            "If a radio button identifier is set, this identifier will be used as selected value instead of the button label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });

        const scoreSlot = this.ref.slots.select<Slots.Numeric>(
            "score",
            "feature"
        );

        this.editor.option({
            name: pgettext("block:radiobuttons", "Score"),
            form: {
                title: pgettext("block:radiobuttons", "Score"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "score", undefined)
                    )
                        .precision(scoreSlot?.precision || 0)
                        .digits(scoreSlot?.digits || 0)
                        .decimalSign(scoreSlot?.decimal || "")
                        .thousands(
                            scoreSlot?.separator ? true : false,
                            scoreSlot?.separator || ""
                        )
                        .prefix(scoreSlot?.prefix || "")
                        .prefixPlural(scoreSlot?.prefixPlural || undefined)
                        .suffix(scoreSlot?.suffix || "")
                        .suffixPlural(scoreSlot?.suffixPlural || undefined),
                ],
            },
            activated: true,
            locked: scoreSlot ? true : false,
            disabled: scoreSlot ? false : true,
        });
    }
}
