/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    arraySize,
    collection,
    editor,
    isFilledString,
    markdownifyToString,
    pgettext,
    tripetto,
} from "tripetto";
import { Radiobuttons } from "..";
import { Radiobutton } from "../radiobutton";

/** Assets */
import ICON from "../../../assets/radiobutton.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:radiobuttons", "Radio button choice");
    },
})
export class RadiobuttonCondition extends ConditionBlock {
    @affects("#condition")
    @collection("#buttons")
    button: Radiobutton | undefined;

    get name() {
        return (
            (this.button ? markdownifyToString(this.button.name) : "") ||
            this.type.label
        );
    }

    get radiobuttons() {
        return (
            (this.node &&
                this.node.block instanceof Radiobuttons &&
                this.node.block.buttons) ||
            undefined
        );
    }

    @editor
    defineEditor(): void {
        if (this.node && this.radiobuttons) {
            const buttons: Forms.IDropdownOption<Radiobutton>[] = [];

            this.radiobuttons.each((button) => {
                if (isFilledString(button.name)) {
                    buttons.push({
                        label: markdownifyToString(button.name),
                        value: button,
                    });
                }
            });

            if (arraySize(buttons) > 0) {
                this.editor.form({
                    title: this.node.label,
                    controls: [
                        new Forms.Dropdown<Radiobutton>(
                            buttons,
                            Forms.Dropdown.bind(this, "button", undefined)
                        ),
                    ],
                });
            }
        }
    }
}
